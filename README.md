# BEACON-Datei des CERL Thesaurus

Die [hier](./data/gnd.txt) zu findende [BEACON-Datei](https://de.wikipedia.org/wiki/Wikipedia:BEACON) verlinkt Normdaten des [CERL Thesaurus](https://data.cerl.org/thesaurus/_search) mit Identifikatoren der [Gemeinsamen Normdatei](https://www.dnb.de/gnd) (GND). Bereitgestellt wird sie über die [Webseite](http://documents.cerl.org/beacon/gnd.txt) des Consortium of European Research Libraries (CERL).

Siehe auch die [Dokumentation zu Linked Data des CERL Thesaurus](https://www.cerl.org/resources/cerl_thesaurus/linkeddata) auf den Seiten des Konsortiums.

## Lizenz

„The CERL Thesaurus data is available under the terms of [Etalab's Open Licence](./LICENSE), which can be considered equivalent to ODC-BY and CC-BY 2.0.“
